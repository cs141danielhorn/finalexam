#include <iostream>
using namespace std;

//Question #1 - Function to computer the average of numbers in a floating 
//point array
float average(int* theArray, int nElements)
{
	float sum = 0;
	float averageOfArray = 0; 
	for (int currentFloat = 0; currentFloat < nElements; currentFloat++)
	{
		sum = sum + theArray[currentFloat];
	}
	averageOfArray = sum / nElements;
	return averageOfArray;
}

//Question #2 - Function to convert inches to both feet and yards
float* inchesToFeetAndYards(float inches)
{
	const float INCHES_IN_FOOT = 12.0f;
	const float INCHES_IN_YARD = 36.0f;
	float numberOfFeet = inches / INCHES_IN_FOOT;
	float numberOfYards = inches / INCHES_IN_YARD;
	float measurements[] = { numberOfFeet, numberOfYards };
	return measurements;
}

//Question #3 - Function to reverse the order of any even integers in an array
//of integers															 
int reverseEven(int* theArray, int nElements)
{
	int* startPointer = theArray;
	int* endPointer = theArray + (nElements - 1);
	int temporary;
	while (startPointer < endPointer)
	{
		if (((*startPointer % 2) == 0) && ((*endPointer % 2) == 0))
		{
			temporary = *startPointer;
			*startPointer = *endPointer;
			*endPointer = temporary;
		}
		++startPointer;
		++theArray;
		--endPointer;
	}
	return *theArray;
}

//Question #4 - Function to return a pointer to the middle of a string 
int stringLength(char* theString) //First, find out how long the string is 
{
	int sumOfCharacters = 0;
	while (*theString != 0)
	{
		sumOfCharacters = sumOfCharacters + 1;
		++theString;
	}
	return sumOfCharacters;
}
char* pointToTheMiddle(char* theString)
{
	char* middlePointer;
	int middleOfString = (stringLength(theString) / 2);
	for (int i = 0; i <= middleOfString; i++)
	{
		if (i == middleOfString)
		{
			*middlePointer = middleOfString;
		}
		else
		{
			++theString;
		}
	}
	return middlePointer;
}

//Question #5 - Function to return a dynamically allocated 2-dimensional array 
//of integer pointers and set all values in the array to 0
int** allocate2DPointers(int nRows, int nColumns)
{
	int** my2DArray = new int*[nColumns];
	for (int row = 0; row < nRows; row++)
	{
		for (int column = 0; column < nColumns; column++)
		{
			my2DArray[row][column] = 0;
		}
	}
	return my2DArray;
}

//Question #6 - Function to remove all occurrences of a specified character
//from a string
char* removeChar(char* theString, char removedLetter)
{
	while (*theString != 0)
	{
		if (*theString == removedLetter)
		{
			*theString = ' '; 
		}
		++theString;
	}
	return theString;
}

//Question #7 - Function that returns a pointer to a dynamically allocated 
//character array that contains a copy of the string with each character 
//repeated a specified number of times
char** repeatTheChars(const char* theString, int numberOfRepetitions)
{
	char** repeatedString = new char*[numberOfRepetitions];
	for (int row = 0; row < numberOfRepetitions; row++)
	{
		repeatedString[row] = repeatedString[row * numberOfRepetitions];
	}
	return repeatedString;
}

//Question #8 - Function to return a dynamically allocated array of floats such
//that the ith element in the returned array is the value in the second array 
//that is closest to the ith element in the first array
float** closestValue(float* firstArray, float* secondArray)
{
	const int SIZE_OF_ARRAYS = 4;
	float** arrayOfCloseness = new float*[SIZE_OF_ARRAYS];
	for (int i = 0; i < SIZE_OF_ARRAYS; i++) //For every element in the first array
	{
		int closestSoFar;
		for (int j = 0; j < SIZE_OF_ARRAYS; i++) //For every element in the second array
		{
			i
		}
	}
}

int main()
{
	//For question #1
	int a[] = { 1, 2, 3, 4, 5 };
	const int SIZE_OF_A = 5;
	float thisIsTheAverage = average(a, SIZE_OF_A);
	cout << thisIsTheAverage << endl;

	//For question #2 
	const float NUMBER_OF_INCHES = 36;
	float* returnedMeasurements = inchesToFeetAndYards(NUMBER_OF_INCHES);
	cout << returnedMeasurements;

	//For question #3 
	int myArray[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
	int reversedArray = reverseEven(myArray, 8); 
	cout << reversedArray << endl;

	//For question #4
	char example[] = "This is the example.";
	char* theMiddle = pointToTheMiddle(example);
	cout << theMiddle << endl;

	//For question #5
	const int N_ROWS = 3;
	const int N_COLUMNS = 4;
	int** allocated2DArray = allocate2DPointers(N_ROWS, N_COLUMNS);
	for (int row = 0; row < N_ROWS; row++)
	{
		delete[] allocated2DArray[row];
	}
	delete[] allocated2DArray;


	//For question #6
	char exampleString[] = "hello world!";
	char* stringAfterRemoval = removeChar(exampleString, 'l');
	cout << stringAfterRemoval;

	//For question #7
	char** repeatedCharacters = repeatTheChars("abc", 5);

	//For question #8
	float first[] = { 1.0f, 10.0f, 100.0f, -3.0f };
	float second[] = { 2.0f, 2000.0f, -1.0f, 11.0f };


	system("pause");
}